# 约拍项目静态页面模板

#### 介绍
一个uniapp+vue的约拍项目模板，已经实现大部分页面

## 推荐项目
**仿小红书web端前端静态页面地址**
- Gitee地址：https://gitee.com/xzjsccz/xiaohongshu

**仿小红书web端完整前后端项目**
- Gitee地址：https://gitee.com/xzjsccz/yanhuo-springboot-vue

**excel校验工具项目地址**
- Gitee地址：https://gitee.com/xzjsccz/cc-excel-util

【联系我】**QQ群：879599115**  
#### 页面展示
|                        移动端                         |                                                       |
| :----------------------------------------------------: | :---------------------------------------------------: |
|   ![image text](./imgs/FF56B4FEA557E0CF81CED6BA1EC4366D.png)    |   ![image text](./imgs/EA8D328FB52D3E6A9F3F2574AF63FD3D.png)      |   
|      ![image text](./imgs/A24F00C8024B6061B8F6E161D046B7F9.png)      |   ![image text](./imgs/B3791141AAF9683C5118658318AACEFB.png)   |
|       ![image text](./imgs/7695F29352C087CFCD628CE5BCEBC2F1.png)       |     ![image text](./imgs/11378CF803D507754B3AB479E0CE3C32.png)     |
|    ![image text](./imgs/56699863AB66E46C7579B9451D87FD65.png)    |     ![image text](./imgs/A9C0994A799ABAE21C3D37FD9FF74999.png)      |
|      ![image text](./imgs/DFD9E5882909A175EFC16423EA66C1E5.png)      |    ![image text](./imgs/E767E2090D099641CFE263F4E3648857.png)     |
|      ![image text](./imgs/5619D7B9340329D71543B088D737964F.png)       |    ![image text](./imgs/7041FE8F33F331D4C5C00FFDE8DE0060.png)    |
